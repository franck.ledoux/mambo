# MAMBO

The MAMBO project, for Model dAtabase Mesh BlOcking, aims to provide a repository for storing  geometric model to study and compare hexahedral block meshing algorithms.

Geometric models are stored in STEP format and are classified as being *basic*, *simple* or *medium* and are stored in the corresponding directories.

The **Script** directory contains Python scripts that are used to generate tetrahedral meshes from any step file using the GMSH Python binding (see *gen_tet_mesh.py*), to generate pictures of a VTK mesh using Paraview Python binding, to generate the **report.pdf** that gather pictures of all the models provided in MAMBO.

## Basic models

Basic models are made using simple geometric primitives and Boolean operations.Simple

## Simple models
1
CAD parts that are not so complex and are only build using Boolean operations as basic models.

## Medium models

More complicated CAD parts with many holes and chamfer parts.

**Note:** The right way to build the *report.pdf* file is to create a **Build** directory as a subdirectory of the MAMBO root directory and to invocate successively *python ../Scripts/gen_pictures.py* then *python ../Scripts/gen_latex.py".